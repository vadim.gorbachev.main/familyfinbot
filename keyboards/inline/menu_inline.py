from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton

menu_inline_markup = InlineKeyboardMarkup(row_width=2,
                                          inline_keyboard=[
                                       [
                                           InlineKeyboardButton(
                                               text='Внести расход',
                                               callback_data='outcome_button'
                                           )
                                       ],
                                       [
                                           InlineKeyboardButton(
                                               text='Внести доход',
                                               callback_data='income_button'
                                           )
                                       ]

                                   ])

menu_cancel_inline_markup = InlineKeyboardMarkup(row_width=1,
                                                 inline_keyboard=[
                                                     [
                                                         InlineKeyboardButton(
                                                             text='Отмена',
                                                             callback_data='cancel'
                                                         )
                                                     ]
                                                 ])
