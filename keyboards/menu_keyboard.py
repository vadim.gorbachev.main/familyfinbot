from aiogram.types import ReplyKeyboardMarkup, KeyboardButton


lets_start = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='Начать')
        ]
    ],
    resize_keyboard=True
)

menu = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='внести расход')
        ],
        [
            KeyboardButton(text='внести доход')
        ]
    ],
    resize_keyboard=True
)
