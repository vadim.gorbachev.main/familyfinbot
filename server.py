# have rewrite middlewares(add db to it) and on_startup function
import logging

from aiogram import Bot, Dispatcher, executor
from aiogram.contrib.fsm_storage.memory import MemoryStorage
# from aiogram.contrib.fsm_storage.redis import RedisStorage2

# import middlewares
from data import config
from admin.notify_admins import on_startup_notify
# from utils.db_api.postgresql import PostgreDataBase
# from utils.db_api.sqlite import DataBase


# Configure logging
logging.basicConfig(level=logging.INFO)

# Init bot, dispatcher, storage and database
bot = Bot(token=config.API_TOKEN)
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)
# db = PostgreDataBase(loop=dp.loop)


async def on_startup(dp):
    # activate middlewares
    # middlewares.setup(dp)
    # admin notification
    await on_startup_notify(dp)
    try:
        await db.create_table_users()
    except Exception as error:
        logging.exception(error)

if __name__ == '__main__':
    from handlers import dp

    executor.start_polling(dp, on_startup=on_startup, skip_updates=True)
