import sqlite3

from aiogram.dispatcher.filters import Text
from aiogram.dispatcher import FSMContext
from aiogram.types import ReplyKeyboardRemove
from aiogram import types

from server import dp, bot, db
from keyboards.inline import menu_inline_markup, menu_cancel_inline_markup
from messages.base import GET_INCOME_MESSAGE, GET_OUTCOME_MESSAGE


@dp.message_handler(commands=['menu'])
@dp.message_handler(Text(equals=['menu', 'Начать', 'меню', 'начать']))
@dp.callback_query_handler(lambda callback_query: callback_query.data == 'menu_button')
async def show_menu(callback_query: types.CallbackQuery):
    await bot.send_message(callback_query.from_user.id, 'Основные действия:\n', reply_markup=menu_inline_markup)
    try:
        db.create_user(id=callback_query.from_user.id, name=callback_query.from_user.full_name)
    except sqlite3.IntegrityError as error:
        print(error)


@dp.callback_query_handler(lambda callback_query: callback_query.data == 'outcome_button')
async def get_outcome(callback_query: types.CallbackQuery, state: FSMContext):
    await bot.answer_callback_query(callback_query.id, cache_time=60)
    await bot.send_message(
            callback_query.from_user.id,
            GET_OUTCOME_MESSAGE,
            reply_markup=menu_cancel_inline_markup
            )
    await state.set_state('outcome')


@dp.callback_query_handler(lambda callback_query: callback_query.data == 'income_button')
async def get_income(callback_query: types.CallbackQuery, state: FSMContext):
    await bot.answer_callback_query(callback_query.id, cache_time=60)
    await bot.send_message(
            callback_query.from_user.id,
            GET_INCOME_MESSAGE,
            reply_markup=menu_cancel_inline_markup
            )
    await state.set_state('income')


@dp.callback_query_handler(lambda callback_query: callback_query.data == 'cancel')
async def cancel(callback_query: types.CallbackQuery):
    await callback_query.answer('Вы отменили последнее действие', show_alert=True)
    await callback_query.answer('Попробуйте сначала. Нажмите кнопку "Начать" или введите /menu')
    await callback_query.message.edit_reply_markup()


# have to add parse functions
@dp.message_handler(state='outcome')
async def add_outcome(message: types.Message, state: FSMContext):
    await message.answer('Ваш расход добавлен в базу')
    await state.finish()


@dp.message_handler(state='income')
async def add_outcome(message: types.Message, state: FSMContext):
    await message.answer('Ваш доход добавлен в базу')
    await state.finish()
