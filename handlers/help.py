from server import dp, bot

from aiogram import types
from aiogram.dispatcher.filters.builtin import CommandHelp


# have to fix
@dp.message_handler(CommandHelp())
@dp.callback_query_handler(lambda callback_query: callback_query.data == 'about_button')
async def help_bot(message: types.Message = None, callback_query: types.CallbackQuery = None):
    text = [
        'Команды: ',
        '/start  —  Начать общение с ботом',
        '/help   —  Получить подсказку',
        '/menu   —  Основные функции бота'
    ]
    if callback_query:
        await bot.answer_callback_query(callback_query.from_user.id, '\n'.join(text))
    if message:
        await message.answer('\n'.join(text))
