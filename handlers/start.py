import sqlite3

from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Command
from aiogram.dispatcher.filters.builtin import CommandStart

from server import dp, db
from keyboards.inline.start_inline import start_inline_markup
from messages.base import START_MESSAGE


# have to rewrite start message
@dp.message_handler(CommandStart())
async def start_bot(message: types.Message):
    await message.answer(START_MESSAGE, reply_markup=start_inline_markup)


# have to fix
@dp.message_handler(Command('email'))
async def add_email(message: types.Message, state: FSMContext):
    await message.answer('Напиши свой e-mail, пожалуйста!')
    await state.set_state('email')


@dp.message_handler(state='email')
async def enter_email(message: types.Message, state: FSMContext):
    email = message.text
    db.update_email(id=message.from_user.id, email=email)
    user = db.select_user(id=message.from_user.id)
    await message.answer(f'Данные обновлены. Результат: {user}')
    await state.finish()
