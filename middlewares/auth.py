from aiogram import types
from aiogram.dispatcher.middlewares import BaseMiddleware
from aiogram.dispatcher.handler import CancelHandler


'''
 Temporary access only by admin_id.
 Have not finished and tested yet.
 Need to rewrite for access by each user_id maybe by own token
 for group
'''


class AuthAccessMiddleware(BaseMiddleware):
    def __init__(self, access_id: int):
        self.access_id = access_id
        super().__init__()

    async def on_process_message(self, message: types.Message, _):
        if int(message.from_user.id) != int(self.access_id):
            await message.answer('Access Denied')
            raise CancelHandler()
