from data.config import ADMIN_ID
from .Auth import AuthAccessMiddleware

from aiogram import Dispatcher


# all middlewares activation
def setup(dp: Dispatcher):
    dp.middleware.setup(AuthAccessMiddleware(ADMIN_ID))
