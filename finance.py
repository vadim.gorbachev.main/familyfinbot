import re
from typing import NamedTuple

from data.db_commands import finance_commands as db_finance
from data.db_commands import users_commands as db_user


class ExpenseData(NamedTuple):
    value: int
    category: str


# have to add errors messages
def parse_message(raw_message: str):
    reg_exp = re.match(r'([\d]+) (.*)', raw_message)
    if not reg_exp or not reg_exp.group(0) or not reg_exp.group(1) or not reg_exp.group(2):
        raise Exception
    try:
        value = int(reg_exp.group(1).replace(' ', ''))
    except Exception as error:
        raise Exception
    category = reg_exp.group(2).strip().lower()
    return ExpenseData(value=value, category=category)


def set_budjet(chat_id: int, budjet: int):
    return db_user.add_budjet(chat_id=chat_id, budjet=budjet)


def add_transaction(chat_id: int, transaction_data: ExpenseData, is_expanse: bool = True):
    add = db_finance.add_outncome if is_expanse else db_finance.add_income
    return add(
        chat_id=chat_id,
        value=transaction_data.value,
        category=transaction_data.category
    )


def get_operations_by_period(chat_id: int, period: str):
    pass


def get_statistics_by_period(chat_id: int, period: str):
    pass


def send_statistics_file(chat_id: int, period: str, format: str):
    pass


