from sqlalchemy import sql

from .schemas import TimeBaseModel, db



class User(TimeBaseModel):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    chat_id = db.Column(db.Integer)
    # name = db.Column(db.String(255))
    budjet = db.Column(db.Integer)

    query: sql.Select


class Outcomes(TimeBaseModel):
    __tablename__ = 'outcomes'

    id = db.Column(db.Integer, primary_key=True)
    chat_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    value = db.Column(db.Integer)
    category = db.Column(db.String(255))

    query: sql.Select


class Incomes(TimeBaseModel):
    __tablename__ = 'incomes'

    id = db.Column(db.Integer, primary_key=True)
    chat_id = db.Column(db.Integer, db.Foreignkey('users.id'))
    value = db.Column(db.Integer)
    category = db.Column(db.String(255))

    query: sql.Select
    # chat_id = Column(Integer)
    name = db.Column(db.String(255))
    budjet = db.Column(db.Integer)

    query: sql.Select
