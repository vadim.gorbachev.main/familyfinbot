from asyncpg import UniqueViolationError

from ..db_models.models import User, db


async def add_user(id: int, name: str, budjet: int = 0):
    try:
        user = User(id=id, name=name, budjet=budjet)
        await user.create()
    except UniqueViolationError:
        # have to add exceptions and warn/err messages
        pass


async def select_all_users():
    return await User.query.gino.all()


async def select_user(id: int):
    return await User.query.where(User.id == id).gino.first()


async def count_users():
    return await db.func.count(User.id).gino.scalar()


async def set_budjet(id: int, budjet: int):
    user = await User.get(id)
    await user.update(budjet=budjet).apply()




