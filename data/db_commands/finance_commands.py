from asyncpg import UniqueViolationError

from data.db_models.schemas import db
from data.db_models.models import Incomes, Outcomes, User


async def add_income(chat_id: int, value: int, category: str = ''):
    try:
        income_item = Incomes(
            # id=id,
            chat_id=chat_id,
            value=value,
            category=category
        )
        await income_item.create()
    except UniqueViolationError:
        pass


async def add_outncome(chat_id: int, value: int, category: str = ''):
    try:
        outcome_item = Outcomes(
            # id=id,
            chat_id=chat_id,
            value=value,
            category=category
        )
        await outcome_item.create()
    except UniqueViolationError:
        pass


async def select_all_incomes():
    return await Incomes.query.gino.all()


async def select_all_user_incomes(chat_id: int):
    return await Incomes.select('value').where(Incomes.chat_id == chat_id).gino.all()


# async def sum_all_user_incomes(chat_id: int):
#     return await Incomes.func


async def select_last_day_user_incomes(chat_id: int):
    return await Incomes.query.where()
